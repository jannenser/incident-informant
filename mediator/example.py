from influxdb import InfluxDBClient
# Example of creating database
client = InfluxDBClient(host='localhost', 
                        port=8086, 
                        username='root',
                        password='root', 
                        database='mediator')
client.create_database('mediator')

# Example of creating queries to database
json_body = [
    {
        "measurement": "test_measurement",
        "tags": {
            "host": "server01",
            "region": "us-west"
        },
        "time": "2009-11-10T23:00:00Z",
        "fields": {
            "value": 0.64,
            "anythingwhatiwant": 'testingvalue',
        }
    }
]
client.write_points(json_body)
result = client.query('select * from test_measurement;')

print(result)