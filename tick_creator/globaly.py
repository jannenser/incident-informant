TICK_PATTERNS = {
        'tick_mean' : '''
var info float
var warn float
var crit float
var info_reset float
var warn_reset float
var crit_reset float
var measure string
var field string
stream
    |from()
        .measurement(measure)
        .groupBy('{}')
    |window()
        .period(1m)
        .every(1m)
    |mean('usage_idle')
    |eval(lambda: 100.0 - "mean")
        .as('used')
        .keep()
    |alert()
        .info(lambda: field > info)
        .infoReset(lambda: field < info_reset)
        .warn(lambda: field > warn)
        .warnReset(lambda: field < warn_reset)
        .crit(lambda: field > crit)
        .critReset(lambda: field < crit_reset)
        .post('http://server:5000/alert')
''',

# keep 2d script like that temporary
'tick_deriv' : '''
var info float
var warn float
var crit float
var measure string
stream
    |from()
        .measurement(measure)
        .groupBy('{}')
    |derivative('io_time')
        .unit(1s)
        .nonNegative()
        .as('diff')
    |influxDBOut
        .database('calculated')
        .create()
    |alert
        .id('{{ index .Tags {}}}/{}_used')
        .message('{{ .ID }}:{{ index .Fields "diff" }}')
        .info(lambda: "used" > info)
        .warn(lambda: "used" > warn)
        .crit(lambda: "used" > crit)
        .post('http://server:5000/alert')
''',
'tick_sum': '''
var info float
var warn float
var crit float
var info_reset float
var warn_reset float
var crit_reset float
var field string
var measure string
stream
    |from()
        .measurement(measure)
        .groupBy('{}')
    |window()
        .period(10s)
        .every(10s)
    |sum('usage_idle')
        .as('sum')
    |alert
        .info(lambda: field > info)
        .infoReset(lambda: field < info_reset)
        .warn(lambda: field > warn)
        .warnReset(lambda: field < warn_reset)
        .crit(lambda: field > crit)
        .critReset(lambda: field < crit_reset)
        .post('http://server:5000/alert')
'''
}

TYPES = {'mean': TICK_PATTERNS['tick_mean'],
         'derivative': TICK_PATTERNS['tick_deriv'],
         'maximum': TICK_PATTERNS['tick_sum']}
kapacitor_urls = ['http://kapacitor:8086']
alert_system_url = 'http://alert_system:9091'
udf_server = 'http://server:5000'
predictor_url = 'http://predictor:9097/create_predictor'
SIZE = 10

