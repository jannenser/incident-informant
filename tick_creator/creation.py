import sqlite3

conn = sqlite3.connect('ARGUMENTS.db')
cursor = conn.cursor()
cursor.execute('''CREATE TABLE NEW_ARGS (
             task_id    TEXT PRIMARY KEY,
             template   TEXT, 
             measurement    TEXT, 
             tag    TEXT,
             field  TEXT)''')
conn.commit()
conn.close()
