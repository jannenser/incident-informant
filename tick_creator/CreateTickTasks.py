import json
import random
import sqlite3
import string
import requests
from flask import Flask
from flask import request
from flask import jsonify
from globaly import *

app = Flask(__name__)

def create_id(size):
    """ creates special id"""
    this_id = ''.join(random.choice(string.ascii_letters + string.digits) for i in range(size))
    return this_id

def get_fields_from_admin(admins_fields):
    """ won't be used in future"""
    fields = json.loads(admins_fields)
    return fields

def create_tick_task(fields):
    """fills arguments in script"""
    template = fields["template"] # determine which tick script to use
    field = fields["field"]
    prediction = fields.get("prediction", False)
    if template == "mean":
        measure = fields.get("measurement", "cpu")
        tag = fields.get("tag", "host")
    elif template == "derivative":
        measure = fields.get("measurement", "cpu")
        tag = fields.get("tag", "*")
    elif template == "sum":
        measure = fields.get("measurement", "mem")
        tag = fields["tag"]
    if prediction:
        k = fields["k"]
        n = fields["n"]
        T = fields["T"]
        start = fields["start"]
        # send to predictor component
    info = fields["info"]
    info_reset = fields["info_reset"]
    warn = fields["warn"]
    warn_reset = fields["warn_reset"]
    crit = fields["crit"]
    crit_reset = fields["crit_reset"]
    level_id = fields["level_id"]

    # connecting with database
    conn = sqlite3.connect('ARGUMENTS.db')
    cur = conn.cursor()

    # checking whether task with given arguments already exists
    cur.execute('''SELECT task_id FROM NEW_ARGS WHERE template = "{}"
            AND measurement = "{}" AND tag = "{}" AND field = "{}"'''.format(template, measure, tag, field))
    row = cur.fetchall()
    # if true, won't create the same task, just save the number of the existing one
    if row:
        task_id = row[0]
        fields.update({"task_id": task_id})
        requests.post(udf_server + "/add", json=fields)
        return task_id
    # else create new task
    tick_script = TYPES[template].format(tag)
    task_id = create_id(SIZE)
    cur.execute('''INSERT INTO NEW_ARGS (task_id, template, measurement, tag, field)
            VALUES (?, ?, ?, ?, ?)''', (task_id, template, measure, tag, field))
    conn.commit()
    conn.close()
    # sends tick task to kapacitor to be executed
    data = {"id": task_id,
            "type": "stream",
            "dbrps": [{"db": "telegraf", "rp": "autogen"}],
            "script": tick_script,
            "vars": {
                "measure": {
                    "value": measure,
                    "type": "string"
                    },
                "info": {
                    "value": info,
                    "type" : "float"
                    },
                "field": {
                    "value": field,
                    "type": "string"
                    },
                "info_reset": {
                    "value": info_reset,
                    "type": "float"
                    },
                "warn": {
                    "value": warn,
                    "type" : "float"
                    },
                "warn_reset": {
                    "value": warn_reset,
                    "type": "float"
                    },
                "crit": {
                    "value": crit,
                    "type" : "float"
                    },
                "crit_reset": {
                    "value": crit_reset,
                    "type": "float"
                    }
                }
        }
    if prediction:
        for_predictor = {"id": task_id,
                        "n": n,
                        "k": k,
                        "T": T,
                        "start": start}
        requests.post(predictor_url, json=for_predictor)
    for k_url in kapacitor_urls:
        resp = requests.post(k_url + '/kapacitor/v1/tasks', json=data)
        print(resp.json())
    return task_id

@app.route('/create_task', methods=['POST'])

def create_task():
    """for testing"""
    data = request.json
    task_id = create_tick_task(data)
    return task_id

@app.route('/send_alert', methods=['POST'])

def send_alert():
    """for alert system sending"""
    data = json.loads(request.get_data())
    message = data['message']
    i = message.rfind(':')
    data['message'] = message[:i]
    requests.post(alert_system_url, json=data)
    return "OK\n"

@app.route('/get_running_tasks', methods=['GET'])

def get_running_tasks_ids():
    """ for testing"""
    for k_url in kapacitor_urls:
        responce = requests.get(k_url + "/kapacitor/v1/tasks")
        data = json.loads(responce.json())
    task_list = list()
    for task in data["tasks"]:
        task_list.append(task["id"])
    return jsonify(task_list)

@app.route('/get_patterns', methods=['GET'])

def get_patterns():
    """jsonify patterns"""
    return jsonify(TICK_PATTERNS)

@app.route('/ping', methods=['GET'])

def ping():
    """ for local testing"""
    return "pong\n"

if __name__ == '__main__':
    app.run(host='task_creator', port=9091)

