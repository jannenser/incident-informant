from flask import Flask
from flask import request
import json

app = Flask(__name__)

@app.route('/ping', methods=['GET'])
def pings():
        return 'pong\n'

@app.route('/alert', methods=['POST'])
def add_data():
    data_json = request.get_data()
    print(data_json)
    return "OK/n"

if __name__ == '__main__':
    app.run(host="server", port=5000)

