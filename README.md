На данный момент можно запустить задачу в kapacitor и увидеть, что её данные корректно приходят в http-server. Http-server должен писать лог в котором будет видно приходящие данные.

*Installation*
```
git clone http://bb.prac.atp-fivt.org:8080/scm/ii/kapacitor-data-handling.git
docker network create demo
cd kapacitor-data-handling
git checkout develop
### Придётся открыть несколько терминалов ###
1.
docker run --rm -it --net=demo --name influxdb influxdb
2.
docker run --rm -it --net=demo --volume "${PWD}"/telegraf.config:/telegraf.config:ro --name telegraf telegraf --config /telegraf.config
3.
cd kapacitor
sudo docker run -u"$(id -u)" -v"${PWD}/kapacitor-load":/data/load -v"${PWD}/kapacitor-data":/var/lib/kapacitor -v "${PWD}/kapacitord.config":/etc/kapacitor/kapacitor.conf:ro --net=demo --rm -it --name kapacitor kapacitor
### Только в случае падения
sudo chmod -R +777 ./kapacitor-data
### перезапустить
4.
docker build -t foo ./tick_creator
docker run --net=demo --name=task_creator --rm -it foo
```


Test

```
docker build -t server_alert ./server
docker run --net=demo --name=server --rm -it server_alert
### Здесь будут писаться пришедшие алерты

### Для проверки создания задач
docker exec -it task_creator bash
curl task_creator:9091/ping
curl task_creator:9091/get_patterns
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"template":"mean"}' \
  task_creator:9091/create_task

```

Если всё выполнено успешно, то в логах сервера можно наблюдать тело приходящего из kapasitor http-запроса.
