from flask import Flask
from flask import request
import numpy as np
import sklearn
from sklearn.linear_model import ElasticNet
import json
import time
import json
from TrafficPredictor import TrafficPredictor

app = Flask(__name__)
predictors = {
    "test": TrafficPredictor()
}

@app.route('/ping')
def pings():
        return 'Hello World!'

@app.route('/add_data', methods=['GET', 'POST'])
def add_data():
    if request.method == 'POST':
        data_json = request.get_data()
        data = json.load(data_json)
        variables = data["series"][0]["columns"]
        values = data["series"][0]["values"][0]

        info = dict()

        for i in range(len(variables)):
            info[variables[i]] = values[i]

        with open("info_file.json", "a") as write_file:
            json.dump(info, write_file)
            write_file.write('\n')
        
        return "OK\n"




if __name__ == '__main__':
    app.run()

