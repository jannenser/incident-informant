#!/usr/bin/env python
"""
    Реализация предсказателя отдельной метрики
"""


import time
import pickle
import datetime
from collections.abc import Iterable
import numpy as np
import pandas as pd
import statsmodels.api as sm
import sklearn
from sklearn.linear_model import ElasticNet
from influxdb import InfluxDBClient


class SinglePredictor:
    """
        Основной класс-предсказатель.
        Использование: создаётся свой instance класса для каждой метрики
    """
    def __init__(self,
                 host=None,
                 port=8086,
                 username=None,
                 password=None,
                 database=None,
                 retention_policy=None,
                 measurement_in=None,
                 measurement_out=None,
                 field=None,
                 seed=42,
                 value_increments=None,
                 time_steps=None,
                 datetimes=None,
                 custom_features=None,
                 base_model=ElasticNet,
                 data_path=None,
                 local_dumping=False,
                 use_timesteps=True,
                 start=None,
                 end=None,
                 fmt="%Y-%m-%dT%H:%M:%SZ",
                 **kwargs):
        """
        :param host: host для InfluxDB (в режиме local_dumping=False)
        :param port: port для InfluxDB (в режиме local_dumping=False)
        :param username: username для InfluxDB (в режиме local_dumping=False)
        :param password: password для InfluxDB (в режиме local_dumping=False)
        :param database: database для InfluxDB (в режиме local_dumping=False)
        :param retention_policy: retention policy для InfluxDB (в режиме local_dumping=False)
        :param measurement_in: measurement для входных данных в InfluxDB (при local_dumping=False)
        :param measurement_out: measurement для выходных данныъ в InfluxDB (при local_dumping=False)
        :param field: field (название поля) для InfluxDB (в режиме local_dumping=False)
        :param seed: random_seed для случайной инициализации весов
        :param value_increments: ndarray(np.float64) из приращений
                                целевой величины (используется только в режиме local_dumping=True)
        :param time_steps: ndarray(np.datetime64) из приращений времени
                                измерений (используется только в режиме local_dumping=True)
        :param datetimes: ndarray(str) timestamp метки для измерений
                          (используется только в режиме local_dumping=True)
        :param custom_features: dict[str : ndarray(np.float64)] - словарь
                                дополнительных метрик, используемых в качестве
                                признаков при предсказании
                                (используется только в режиме local_dumping=True)
        :param base_model: базовая модель предсказания
        :param **kwargs: гиперпараметры для базовой модели
        :param data_path: путь к директории для json-объекта класса с данными
        :param local_dumping: нужно ли записывать данные на диск в pickle-формате
        :param use_timesteps: если True, то класс ожидает на вход приращения
                              по времени, а не временные метки
        :param start: начало периода, по которому следует брать
                      данные (в режиме local_dumping=False).
                      Если не задан, то время не ограничивается слева.
        :param end: конец периода, по которому следует брать данные (в режиме local_dumping=False).
                      Если не задан, то время не ограничивается справа.
        :param fmt: string с форматом, в котором приходит время
                    (используется в случае use_timesteps=False)
        """
        self.seed = seed
        self.fmt = fmt
        self.local_dumping = local_dumping
        self.use_timesteps = use_timesteps
        self.base_model = base_model
        self.start = start
        self.end = end
        self.last = None
        self.first = None

        if self.local_dumping:
            if not use_timesteps:
                raise ValueError('Local_dumping mode works with use_timesteps=True only')
            if data_path is None:
                filename = 'TrafficPredictorData' + str(time.time()).replace('.', '_') + '.pickle'
                self.data_path = './' + filename
                file_obj = open(filename, 'wb')
            else:
                self.data_path = data_path
                file_obj = open(data_path, 'wb')

            data = {
                'value_increments': value_increments,
                'time_steps': time_steps,
                'times': time_steps.cumsum() if time_steps is not None else None,
                'datetimes': datetimes,
                'custom_features': custom_features,
            }
            pickle.dump(data, file_obj)
            file_obj.close()
        else:
            if host is None:
                raise TypeError('Parameter host should not be None in not local_dumping mode')
            if username is None:
                raise TypeError('Parameter username should not be None in not local_dumping mode')
            if password is None:
                raise TypeError('Parameter password should not be None in not local_dumping mode')
            if database is None:
                raise TypeError('Parameter database should not be None in not local_dumping mode')
            if retention_policy is None:
                raise TypeError('Parameter retention_policy should not be None \
                                in not local_dumping mode')
            if measurement_in is None or measurement_out is None:
                raise TypeError('Parameters measurement_in and measurement_out should \
                                not be None in not local_dumping mode')
            if field is None:
                raise TypeError('Parameter field should not be None in not local_dumping mode')

            self.client = InfluxDBClient(host=host,
                                         port=port,
                                         username=username,
                                         password=password,
                                         database=database)
            self.host = host
            self.port = port
            self.username = username
            self.password = password
            self.database = database
            self.retention_policy = retention_policy
            self.measurement_in = measurement_in
            self.measurement_out = measurement_out
            self.field = field
            self.prediction_epoch_ = 1


        if self.base_model == sm.tsa.statespace.SARIMAX:
            self.model = base_model(endog=self.parse_data()[1], **kwargs)
            self.params = kwargs
        else:
            self.model = base_model(**kwargs)


    def add(self, new_time_steps, new_value_increments, new_custom_features=None):
        """
        Добавляет новые данные в режиме local_dumping=True. Иначе ничего не делает.
        :param new_increments: ndarray(np.float64) из новых приращений
        :param new_time_steps: ndarray(np.float64) из новых дат измерений
        :param new_custom_features: dict[str : ndarray(np.float64)] - словарь новых
                               дополнительных метрик, используемых в качестве
                               признаков при предсказании
        """
        if self.local_dumping:
            data = self.get_data()

            if new_value_increments.shape != new_time_steps.shape:
                raise ValueError('Shape mismatch: new_value_increments and \
                                 new_dates should have the same shape')
            if data['value_increments'] is None:
                data['value_increments'] = new_value_increments.tolist()
            else:
                data['value_increments'] = np.append(data['value_increments'],
                                                     new_value_increments).tolist()
            if data['time_steps'] is None:
                data['time_steps'] = new_time_steps.tolist()
            else:
                data['time_steps'] = np.append(data['time_steps'], new_time_steps).tolist()

            if data['custom_features'] is None:
                data['custom_features'] = new_custom_features
            elif new_custom_features is not None:
                for key, values in new_custom_features.items():
                    if key in data['custom_features'].keys():
                        data['custom_features'][key] = np.append(data['custom_features'][key],
                                                                 values).tolist()
                    else:
                        data['custom_features'][key] = values.tolist()

            with open(self.data_path, 'wb') as tmp:
                pickle.dump(data, tmp)
        else:
            pass


    def get_data(self, bounds=None):
        """
        Возвращает текущие данные класса
        :param: bounds - дополнительные ограничения на select
                (работает только в режиме local_dumping=False)
        :returns: json-объект для данных класса в local_dumping=True режиме
                  и ndarray (n_obj, 2) иначе
        """
        if self.local_dumping:
            with open(self.data_path, 'rb') as tmp:
                data = pickle.load(tmp)
            return data

        if bounds is not None:
            down_ = bounds[0] if self.start is None else max(self.start, bounds[0])
            up_ = bounds[1] if self.end is None else min(self.end, bounds[1])
            query = 'select {0} from {1}.{2}.{3} where time >= \'{4}\' and time <= \'{5}\' ;'\
            .format(self.field,
                    self.database,
                    self.retention_policy,
                    self.measurement_in,
                    down_,
                    up_)
        else:
            if self.start is None and self.end is None:
                query = 'select {0} from {1}.{2}.{3};'\
                .format(self.field,
                        self.database,
                        self.retention_policy,
                        self.measurement_in)
            elif self.start is None:
                query = 'select {0} from {1}.{2}.{3} where time >= \'{4}\' ;'\
                .format(self.field,
                        self.database,
                        self.retention_policy,
                        self.measurement_in,
                        self.start)
            elif self.end is None:
                query = 'select {0} from {1}.{2}.{3} where time <= \'{4}\' ;'\
                .format(self.field,
                        self.database,
                        self.retention_policy,
                        self.measurement_in,
                        self.end)
            else:
                query = 'select {0} from {1}.{2}.{3} where time >= \'{4}\' and time <= \'{5}\' ;'\
                .format(self.field,
                        self.database,
                        self.retention_policy,
                        self.measurement_in,
                        self.start,
                        self.end)

        result = self.client.query(query)
        result_gen = result.get_points()
        return np.array([x['time'] for x in result_gen]),\
                np.array([x[self.field] for x in result_gen])

    def write_prediction(self, times, values):
        """
        Записывает данные в measuremrnt_out -> field, если self.local_dumping=False
        Иначе ничего не делает
        :param times: временные метки для записи в базу
        :values: значения, которые нужно записать в базу
                (по измерению self.measurement_out в поле self.field)
        """
        if self.local_dumping:
            pass
        else:
            json_body = [
                {
                    "measurement": self.measurement_out,
                    "tags" : {
                        "epoch": self.prediction_epoch_
                    },
                    "time": time,
                    "fields": {
                        self.field: value,
                    }
                }
                for time, value in zip(times, values)
            ]
            self.client.write_points(json_body)

    def write_data(self, data, custom_tag_key, custom_tag_value):
        """
        Записывает данные в measuremrnt_out -> field,
        если self.local_dumping=False, используя переданный tag
        (временные метки устанавливаются автоматически по времени записи).
        Иначе ничего не делает.
        :param data: значения, которые нужно записать в базу
                     (по измерению self.measurement_out в поле self.field)
        :custom_tag_key: tag, по которому следует записать значения в базу
        :custom_tag_value: спецификация tag, которое следует присвоить данным
        """
        if self.local_dumping:
            pass
        else:
            if isinstance(data, Iterable):
                json_body = [
                    {
                        "measurement": self.measurement_out,
                        "tags" : {custom_tag_key: custom_tag_value},
                        "fields": {self.field: value,}
                    }
                    for value in data
                ]
            else:
                json_body = [
                    {
                        "measurement": self.measurement_out,
                        "tags": {custom_tag_key: custom_tag_value},
                        "fields": {self.field: data,}
                    }
                ]
            self.client.write_points(json_body)


    def reset_data(self):
        """
        Обнуляет данные класса, не затрагивая модель предсказания.
        Вне local_dumping=True выполняет только get_data().
        :returns: json-объект для данных, отвязанных от класса в local_dumping режиме
                  и ndarray (n_obj, 2) иначе
        """
        saved = self.get_data()
        if self.local_dumping:
            data = {
                'value_increments': None,
                'time_steps': None,
                'times': None,
                'datetimes': None,
                'custom_features': None,
            }

            with open(self.data_path, "wb") as tmp:
                pickle.dump(data, tmp)
        else:
#             query = 'drop {0} from {1}.{2};'.format(self.measurement,
#                                                     self.database,
#                                                     self.retention_policy)
#             self.client.query(query)
            pass
        return saved

    def parse_data(self, ignore_custom=False, bounds=None,
                   update_first=False, update_last=False):
        """
        Раскладывает данные из self.data_path или influxdb на признаки и целевую переменну
        :param: bounds - дополнительные ограничения на select (для режима local_dumping=False)
        :param ignore_custom - если True, игнорирует признаки в self.custom_features
                                          (для режима local_dumping=True)
        :param update_first, update_last: внутренние флаги для обновления границ,
                                          по которым обучалась модель
        :returns: полученные после парсинга X, y
        """
        data = self.get_data(bounds)
        if self.local_dumping:

            if not ignore_custom and data['custom_features'] is not None:
                times = np.array(data['time_steps']).cumsum()

                features = np.zeros(shape=(data.shape[0], len(data['custom_features']) + 1))
                features[:, 0] = times
                for i, (_, feature) in enumerate(data['custom_features'].items()):
                    features[:, i + 1] = feature
            else:
                features = np.array(data['time_steps']).cumsum().reshape(-1, 1)
            target = np.array(data['value_increments']).reshape(-1, 1)
        else:
            if update_first:
                self.first = data[0][0]
            if update_last:
                self.last = data[0][-1]

            if self.use_timesteps:
                parse_date = lambda x: datetime.datetime.strptime(x, self.fmt)
                get_diff = lambda i, j: parse_date(data[0][i]) - parse_date(data[0][j])
                features = np.array([0.0 if i == 0 else get_diff(i, i - 1).total_seconds() \
                                    for i in range(len(data[0]))]).cumsum().reshape(-1, 1)
            else:
                features = data[0].reshape(-1, 1)
            target = data[1].reshape(-1, 1)
        return features, target

    def transform(self, ignore_custom=False):
        """
        Обучает модель на внутренних данных, записаных в поля класса
        :param ignore_custom: если True, игнорирует признаки в self.custom_features
                              (только для local_dumping=True)
        """
        if self.start is None or self.end is None:
            data, target = self.parse_data(ignore_custom,
                                           update_first=True,
                                           update_last=True)
        else:
            data, target = self.parse_data(ignore_custom,
                                           bounds=(self.start, self.end),
                                           update_first=True,
                                           update_last=True)
        if sm.tsa.statespace.SARIMAX == self.base_model:
            self.model = sm.tsa.statespace.SARIMAX(endog=target, **self.params)
            self.model = self.model.fit()
        else:
            self.model.fit(data, target)

    def add_transform(self, new_time_steps, new_value_increments,
                      new_custom_features=None, ignore_custom=False):
        """
        Добавляет новые данные и обучает модель на внутренних данных, записаных в поля класса
        Если local_dumping=False, выполняет только transform()
        :param ignore_custom: если True, игнорирует признаки в self.custom_features
        """
        self.add(new_time_steps, new_value_increments, new_custom_features)
        self.transform(ignore_custom)

    def predict(self, mode, residium_time=None,
                seq_len=None, time_steps=None,
                return_full_seq=True):
        """
        Осуществляет предсказание модели
        :param time_steps: приращения времени для предсказания, если self.use_timesteps=True,
                           иначе - лист временных меток в формате self.fmt.
                           Обязателен для mode='prediction'
        :param seq_len: кол-во измерений за один цикл расчёта трафика
        :param residium_time: остаток времени до перерасчёта трафика,
                              обязателен для mode=='extrapolate'
        :param mode: 'extrapolate' или 'prediction' - экстраполировать последовательность
                      или предсказать для следующих шагов time_steps
        :param return_full_seq: если True, возвращает всю последовательность
                                (train @ pred), иначе - только pred
        """
        data, target = self.parse_data(bounds=(self.first, self.last))

        if mode == 'extrapolate':
            if residium_time is None:
                raise AttributeError('residium_time should be defined in extrapolate mode')
            if seq_len is None:
                raise AttributeError('seq_len should be defined in extrapolate mode')
            if not isinstance(seq_len, int):
                raise AttributeError('seq_len should be integer in extrapolate mode')

            n_obj = target.shape[0]

            if seq_len <= n_obj:
                raise AttributeError('seq_len should be more than sample size in extrapolate mode')

            residium_measurements = seq_len - n_obj

            if not self.local_dumping:
                last = datetime.datetime.strptime(self.last, self.fmt)
                end = last + datetime.timedelta(seconds=residium_time)
                step = datetime.timedelta(seconds=residium_time / residium_measurements)
                future = pd.date_range(start=last.strftime(self.fmt),
                                       end=end.strftime(self.fmt),
                                       freq=step).strftime(self.fmt)[1:]

            if self.use_timesteps:
                if self.local_dumping:
                    last = data[:, 0][-1]
                else:
                    last = data[-1]

                times = np.linspace(last,
                                    residium_time,
                                    residium_measurements,
                                    endpoint=True)
            else:
                times = future


            if sm.tsa.statespace.SARIMAX == self.base_model:
                diff_pred = self.model.get_prediction(dynamic=False)
                pred = diff_pred.predicted_mean
            else:
                pred = self.model.predict(times.reshape(-1, 1))

            if return_full_seq:
                if not self.local_dumping:
                    past = self.get_data(bounds=(self.first, self.last))[0]
                    self.write_prediction(np.append(past, future), np.append(target, pred))
                    self.prediction_epoch_ += 1
                return np.append(target, pred)

            if not self.local_dumping:
                self.write_prediction(future, pred)
                self.prediction_epoch_ += 1
            return pred

        if mode == 'prediction':
            if time_steps is None:
                raise AttributeError('time_steps should be defined in prediction mode')
            if self.use_timesteps:
                if self.local_dumping:
                    last = data[:, 0].cumsum()[-1]
                else:
                    last = data[-1]
                times = last + time_steps.cumsum()
            else:
                times = time_steps
            pred = self.model.predict(times.reshape(-1, 1))
            if not self.local_dumping:
                if self.use_timesteps:
                    first = datetime.datetime.strptime(self.first, self.fmt).timestamp()
                    future = [datetime.datetime.fromtimestamp(t).strftime(self.fmt)\
                             for t in first + times]
                    self.write_prediction(future, pred)
                    self.prediction_epoch_ += 1
                else:
                    self.write_prediction(times, pred)
                    self.prediction_epoch_ += 1
            return pred

        raise AttributeError('mode should be "prediction" or "extrapolate"')


    def predict_quantile(self, order, seq_len, residium_time, mode='fraction'):
        """
        Предсказывает квантиль счётчика
        :param order: порядок квантили (либо это доля, либо процент - в зависимости от mode)
        :param seq_len: кол-во измерений
        :param residium_time: остаток времени до перерасчёта трафика
        :param mode: 'fraction' или 'percent' - задаёт способ определения квантили
        """
        pred = self.predict(mode='extrapolate', residium_time=residium_time,
                            seq_len=seq_len, return_full_seq=True).cumsum()
        if mode == 'fraction':
            quantile = np.quantile(pred, order)
            if not self.local_dumping:
                self.write_data(quantile, custom_tag_key='epoch',
                                custom_tag_value=self.prediction_epoch_)
            return quantile
        if mode == 'percent':
            percentile = np.percentile(pred, order)
            if not self.local_dumping:
                self.write_data(percentile, custom_tag_key='epoch',
                                custom_tag_value=self.prediction_epoch_)
            return percentile
        raise AttributeError('mode should be "fraction" or "percent"')

    def cross_validate(self, *args, ignore_custom=False, **kwargs):
        """
        Вызывает sklearn.model_selection.cross_validate, передавая ему в качестве
        модели self.base_model, а в качестве данных - данные класса
        :param ignore_custom: игнорировать ли пользовательские признаки (custom_features)
        :params args, kwargs: все аргументы, принимаемые sklearn.model_selection.cross_validate
        :returns: score (scoresdict of float arrays of shape (n_splits,))
        """
        data, target = self.parse_data(ignore_custom)
        return sklearn.model_selection.cross_validate(self.base_model(),
                                                      data, target, *args, **kwargs)

    def grid_search(self, *args, ignore_custom=False,
                    random=False, inplace=False, fit=None, **kwargs):
        """
        Вызывает sklearn.model_selection.GridSearchCV или
        sklearn.model_selection.RandomizedSearchCV (в зависимости от значения
        булевого параметра random), передавая ему в качестве
        модели self.base_model, а в качестве данных - данные класса
        :param ignore_custom: игнорировать ли пользовательские признаки (custom_features)
        :param random: осуществляет выбор между 2 поисками
        :param inplace: заменяет текущую модель self.model на gs.best_estimator_
        :param fit:
        :params args, kwargs: все аргументы, принимаемые выбранным поиском
        :returns gs_.cv_results_: dict of numpy (masked) ndarrays
        """
        if 'refit' in kwargs:
            raise AttributeError('TrafficPredictor doesnt use `refit` param, \
                                  use `fit` param instead')
        if inplace:
            if fit is None:
                raise AttributeError('param `fit` has to been filled in inplacing mode')
        data, target = self.parse_data(ignore_custom)
        if not random:
            gs_ = sklearn.model_selection.GridSearchCV(self.base_model,
                                                       *args, **kwargs)
        else:
            gs_ = sklearn.model_selection.RandomizedSearchCV(self.base_model,
                                                             *args, **kwargs)
        gs_.fit(data, target)
        if inplace:
            self.model = gs_.best_estimator_
            if fit:
                self.model.fit(data, target)
        return gs_.cv_results_
